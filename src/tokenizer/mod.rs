use std::collections::VecDeque;
use std::fmt;

/**
 * The tokenizer takes a stream of characters and transforms
 * them into a token stream to be used by the parser to create 
 * instructions to execute.
 */

/// Takes a single string, made up of unicode characters,
/// and tokenizes it. Takes multi lined strings into account.
pub struct Tokenizer {
    /// An internal buffer of things to process. 
    buffer: VecDeque<char>,
}

impl Tokenizer {
    /// Create a new tokenizer with properly initialized fields.
    pub fn new(data: Vec<char>) -> Tokenizer {
        Tokenizer { buffer: VecDeque::from(data), }
    }

    /// Private tokenize function to do the dirty work.
    /// returns a single token or a syntax error.
    fn tokenize_single(&mut self) -> Result<Option<OpToken>, ()> {
        // Get the next term and then validate it.
        
        Ok(None)
    }
    // Scans the input buffer for the next term to tokenize
    fn get_next_term(&mut self) -> Option<Vec<char>> {
        let mut comment = false;
        let mut term : Vec<char> = Vec::new();

        // Get the next term, ignoring whitespace, comments, and commas.
        loop {
            let ch = self.buffer.pop_front().unwrap_or('\0');
            if (comment) && (ch != '\n') { continue; } // If we're inside a comment, skip until the character becomes a newline.
            if ch == ';' { comment = true; continue; } // If we start a comment begin comment mode.
            if ch.is_ascii() && !ch.is_ascii_whitespace() { term.push(ch); continue; } // If we're ascii and not whitespace, we've got a term!
            if (term.len() > 0) && (ch.is_ascii_whitespace() || ch == '\0') { return Some(term); }  // If we don't have anything, keep skipping whitespace.
                                                                                                    // if we do have something, new whitespace means end of term.
            if ch == '\0' { return None; } // If we get a NULL it means we've reached EOF or end of buffer.
        }
    }

    pub fn queue(&mut self, data: Vec<char>) {
        self.buffer.append(&mut VecDeque::from(data));
    }
    pub fn queue_deque(&mut self, data: &mut VecDeque<char>) {
        self.buffer.append(data);
    }
}

/// Represents the two types of tokens that may be operands to an OpToken
#[derive(Debug)]
pub enum Token {
    /// Some constant value. Must be 32 bits in size.
    CONST(u32),
    /// Some register.
    REG(RegToken)
}

/// Tokens representing some register.
#[derive(Debug)]
pub enum RegToken {
    /// Twelve General Purpose Registers, plus flags and IP register.
    /// These take no params.
    R01,
    R02,
    R03,
    R04,
    R05,
    R06,
    R07,
    R08,
    R09,
    R10,
    R11,
    R12,
    R13,
    R14,
    R15,
    R16,
    EFLAGS,
    IP,
}

/// Due to the way rust structs work, each OpToken variant actually
/// contains its own params.
#[derive(Debug)]
pub enum OpToken {
    /// Data Transfer Instructions.
    /// Usually take two other tokens as params.
    MOV{ dst : Token, src: Token },
    XCHG{ dst : Token, src: Token },
    BSWAP{ dst : Token, src: Token },
    PUSH{ op : Token },
    POP{ op : Token },
    PUSHA,
    POPA,
    /// Arithmetic instructions.
    ADCX{ dst : Token, src: Token },
    ADOX{ dst : Token, src: Token },
    ADD{ dst : Token, src: Token },
    ADC{ dst : Token, src: Token },
    SUB{ dst : Token, src: Token },
    SBB{ dst : Token, src: Token },
    IMUL{ dst : Token, src: Token },
    MUL{ dst : Token, src: Token },
    IDIV{ dst : Token, src: Token },
    DIV{ dst : Token, src: Token },
    INC{ op : Token },
    DEC{ op : Token },
    NEG{ op : Token },
    CMP{ dst : Token, src: Token },
    /// Boolean Operators
    AND{ dst : Token, src: Token },
    OR{ dst : Token, src: Token },
    XOR{ dst : Token, src: Token },
    NOT{ dst : Token, src: Token },
    /// Shift and Rotate
    SAR{ op : Token },
    SHR{ op : Token },
    SAL{ op : Token },
    SHL{ op : Token },
    ROR{ op : Token },
    ROL{ op : Token },
    RCR{ op : Token },
    RCL{ op : Token },
    TEST{ dst : Token, src: Token },
    /// Control Tokens
    JMP{ op : Token },
    JE{ op : Token },
    JZ{ op : Token },
    JNE{ op : Token },
    JNZ{ op : Token },
    JA{ op : Token },
    JAE{ op : Token },
    JB{ op : Token },
    JBE{ op : Token },
    JG{ op : Token },
    JGE{ op : Token },
    JL{ op : Token },
    JLE{ op : Token },
    JC{ op : Token },
    JNC{ op : Token },
    JO{ op : Token },
    JNO{ op : Token },
    JS{ op : Token },
    JNS{ op : Token },
    CALL{ op : Token },
    RET,
    IRET,
    INT{ ivec: Token },
    INTO,
    /// I/O Instructions
    INB{ dst : Token, src: Token },
    OUTB{ dst : Token, src: Token },
    INW{ dst : Token, src: Token },
    OUTW{ dst : Token, src: Token },
    INDW{ dst : Token, src: Token },
    OUTDW{ dst : Token, src: Token },
    /// EFLAGS OPS
    STC,
    CLC,
    CMC,
    CLD,
    STD,
    PUSHF,
    POPF,
    STI,
    CLI,
    /// Misc. Ops
    LEA{ op : Token },
    NOP,
    UD,
    CPUID,
}

impl OpToken {
    /// Returns the name of the token as a vec of chars.
    pub fn to_chars(&self) -> Vec<char> {
        // This gets the debug representation of the enum variant.
        // If a variant has no fields, it simply splits on nothing,
        // next returns the only element, and its turned into a charvec.
        // However, if it has fields, the debug representation will include
        // a space between the variant name that we want and the fields.
        // splitting on spaces gets this for us pretty easily.
        return format!("{:?}", self).split(' ').next().unwrap().chars().collect();

        // This is totally a hack but I personally love it.
    }
}