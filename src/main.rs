/**
    Takes a file as a command line argument and
    executes that file in a self-modifying environment.
    Uses a custom architecture mimicking intel x86
    for POC reasons.
*/
#[allow(unreachable_code)]
#[allow(dead_code)]



mod tokenizer;

fn main() {
    let mut tzr = tokenizer::Tokenizer::new("MOV R01, R16 ; register time!".chars().collect());
    for _ in 0..4 {
        println!("{:?}", tzr.get_next_term());
    }
}
/*
/// Contains virtual processor registers.
struct Regs {
    /// Sixteen general-purpose registers of 32 bits (4 bytes) each
    /// These are accessed not directly but through MOV methods.
    /// The registers are named RXX - where XX is the register number.
    /// If the number is less than ten, a zero is prepended e.g. R01 or R02.
    General: [u8; 4*16],

    /// A 32 bit flags register
    Eflags: [u8; 4],

    /// A 32 bit instruction pointer
    Ip: [u8; 4],
}

impl Regs {
    pub fn New() -> Regs {
        Regs{
            General: [0; 4*16],
            Eflags: [0; 4],
            Ip: [0; 4],
        }
    }
}
*/